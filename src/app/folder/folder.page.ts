import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { CursosService } from '../servicios/cursos.service';
import { Curso } from '../curso';
import { StorageService } from '../servicios/storage.service';
import { AppComponent } from '../app.component';
import { FotoService } from '../servicios/foto.service';
import { AlumnosService } from '../servicios/alumnos.service';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  cursos:Curso[];
  logIn:boolean;
  base64Data: string;
  converted_image: string;
  

  constructor(private activatedRoute: ActivatedRoute,
    private cursosService:CursosService,
    public loadingController:LoadingController,
    private router:Router,
    public storageService:StorageService,
    public alertController: AlertController,
    public appComponent:AppComponent,
    public fotoService:FotoService,
    private alumnosService:AlumnosService) { }

    async addPhotoToGallery(){
      const foto = await this.fotoService.addNewToGallery();
      const data={'name':await this.storageService.get('user'),'foto_perfil':foto};
      console.log(data);
      this.alumnosService.setAlumnoFotoPerfil(data).subscribe(res => {
        console.log(res);
      }, err=>{
        console.log(err);
      });
      this.base64Data = foto;
      this.converted_image = "data:image/jpeg;base64,"+this.base64Data;
    }

    async presentAlert() {
      await console.log(this.storageService.get('nota'));
      const alert = await this.alertController.create({
        header: 'Nota Media',
        message: 'La nota media es :'+await this.storageService.get('nota'),
        buttons: ['OK']
      });
      await alert.present();
    }

  async getCursosHome(){
    const loading = await this.loadingController.create({
      message:'Cargando'
    });

    await loading.present();
    await this.cursosService.getCursos().subscribe(res => {
      this.cursos = res;
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }
  async getFoto(){
    const loading = await this.loadingController.create({
      message:'Cargando'
    });
    const data={'id':await this.storageService.get('userid')}
    await loading.present();
    await this.alumnosService.getAlumnoFotoPerfil(data).subscribe(res => {
      const foto = res['foto'];
      console.log(foto);
      this.base64Data = foto;
      this.converted_image = "data:image/jpeg;base64,"+this.base64Data;
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }

  async datos(id){
    if(await this.storageService.get('login')){
      if(await this.storageService.get('admin')){
        this.router.navigateByUrl('/alumnos-curso/'+id);
      }else{
        this.presentAlert();
      }
    }
  }
  async cerrarSession(){
    await this.storageService.set('login',false);
    await this.storageService.set('admin',false);
    this.router.navigateByUrl('login');
    this.appComponent.selectedIndex=2;
  }
  async comprobarSession(){
    this.logIn=await this.storageService.get('login');
  }
  ngOnInit() {
    this.getFoto();
    this.comprobarSession();
    this.getCursosHome();
  }

}

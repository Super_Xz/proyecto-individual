import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AppComponent } from '../app.component';
import { StorageService} from '../servicios/storage.service'
import { Usuario } from '../usuario';

const httpOptions = { headers:new HttpHeaders({'Content-Type':'application/json'})}
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user:string;
  password:string;
  public url = "http://192.168.0.37:8000/ws/usuario"
  credentials={user:'',password:''};


  constructor(public storageService:StorageService,
              private router:Router,
              private httpClient:HttpClient,
              public loadingController:LoadingController,
              public appComponent:AppComponent) { }

  getUsuario(credentials):Observable<any>{
    return this.httpClient.put(this.url,credentials);
  }


  async logIn(){
    this.credentials['user']=this.user;
    this.credentials['password']=this.password;
    const loading = await this.loadingController.create({
      message:'Cargando'
    });

    await loading.present();
    await this.getUsuario(this.credentials).subscribe(async res => {
      if (res!=null && res['admin']){
        console.log('login correcto');
        await this.storageService.set('login',true);
        await this.storageService.set('admin',true);
        await this.storageService.set('user',res['name']);
        await this.storageService.set('userid',res['id']);
        await this.storageService.set('nota','Este usuario no tiene nota');
        await this.router.navigateByUrl('/folder');
        this.appComponent.selectedIndex=0;
      }
      else if(res!=null && !res['admin']){
        console.log('login correcto');
        await this.storageService.set('login',true);
        await this.storageService.set('admin',false);
        await this.storageService.set('user',res['name']);
        await this.storageService.set('userid',res['id']);
        await this.storageService.set('nota',res['notaMedia']);
        await this.router.navigateByUrl('/folder');
        this.appComponent.selectedIndex=0;
      }else{
        console.log('login incorrecto')
      }
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }
  async comprobarSession(){
    if(await this.storageService.get('login')){
      this.router.navigateByUrl('/folder');
      this.appComponent.selectedIndex=0;
    }
  }

  ngOnInit() {
    this.comprobarSession();
  }

  

}

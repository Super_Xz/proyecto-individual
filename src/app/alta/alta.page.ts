import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { CursosService } from '../servicios/cursos.service';
import { AlumnosService } from '../servicios/alumnos.service'
import { Curso } from '../curso';
import { StorageService} from '../servicios/storage.service'
import { Alumno } from '../alumno';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-alta',
  templateUrl: './alta.page.html',
  styleUrls: ['./alta.page.scss'],
})
export class AltaPage implements OnInit {
  cursos:Curso[];
  alumnos:Alumno[];
  public alumno = {'name':'','notaMedia':null,'cursoID':-1};
  datos ={'idalumno':-1,'idCurso':-1};
  cursoID: number;
  alumnoID: any;

  constructor(private activatedRoute: ActivatedRoute,
    private cursosService:CursosService,
    public loadingController:LoadingController,
    private router:Router,
    private alumnosService:AlumnosService,
    public storageService:StorageService,
    public appComponent:AppComponent,
    public alertController:AlertController) { }

  async presentAlert() {
    await console.log(this.storageService.get('nota'));
    const alert = await this.alertController.create({
      header: 'Asignacion correcta',
      message: 'El usuario a sido asignado al curso',
      buttons: ['OK']
    });
    await alert.present();
  }

  async getCursosHome(){
    const loading = await this.loadingController.create({
      message:'Cargando'
    });

    await loading.present();
    await this.cursosService.getCursos().subscribe(res => {
      this.cursos = res;
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }
  async getAlumnos(){
    const loading = await this.loadingController.create({
      message:'Cargando'
    });

    await loading.present();
    await this.alumnosService.getAlumnos().subscribe(res => {
      this.alumnos = res;
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }
  async altaAlumno(){
    this.datos['idCurso']=this.cursoID;
    this.datos['id']=this.alumnoID;
    const loading = await this.loadingController.create({
      message:'Cargando...'
    });
    await loading.present();
    await this.alumnosService.actualizarAlumno(this.datos)
    .subscribe(res =>{
      console.log(res);
      this.presentAlert();
      loading.dismiss();
    }, err =>{
      console.log(err);
      loading.dismiss();
    });
  }
  async comprobarAdmin(){
    if(await this.storageService.get('admin')==false){
      this.router.navigateByUrl('/login');
      this.appComponent.selectedIndex=2;
    }
  }

  ngOnInit() {
    this.comprobarAdmin();
    this.getCursosHome();
    this.getAlumnos();
  }

  enviarFormulario(){
    this.altaAlumno();
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlumnosCursoPage } from './alumnos-curso.page';

const routes: Routes = [
  {
    path: '',
    component: AlumnosCursoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlumnosCursoPageRoutingModule {}

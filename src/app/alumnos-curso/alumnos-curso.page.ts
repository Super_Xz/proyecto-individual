import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Alumno } from '../alumno';
import { AlumnosService } from '../servicios/alumnos.service';

@Component({
  selector: 'app-alumnos-curso',
  templateUrl: './alumnos-curso.page.html',
  styleUrls: ['./alumnos-curso.page.scss'],
})
export class AlumnosCursoPage implements OnInit {
  cursoID:string
  idCurso={id:''};
  alumnos:Alumno[];
  constructor(private route: ActivatedRoute,
              private alumnosService:AlumnosService,
              public loadingController:LoadingController,
              private router:Router,) { }
  volver(){
    this.router.navigateByUrl('folder')
  }
  async getAlumnosCurso(){
    this.idCurso={id:this.cursoID};
    const loading = await this.loadingController.create({
      message:'Cargando'
    });

    await loading.present();
    await this.alumnosService.getAlumnosCurso(this.idCurso).subscribe(res => {
      this.alumnos = res;
      loading.dismiss();
    }, err=>{
      console.log(err);
      loading.dismiss();
    });
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      this.cursoID = params.get('id');
    });
    this.getAlumnosCurso();
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http'
import {Observable, of, throwError} from 'rxjs';
import { catchError,tap,map} from "rxjs/operators";
import { Alumno} from '../alumno'

const httpOptions = { headers:new HttpHeaders({'Content-Type':'application/json'})}

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {
  public url = "http://192.168.0.37:8000/ws/alumnado"

  constructor(private httpClient:HttpClient) { }

  crearAlumno(alumno:Alumno):Observable<any>{
    return this.httpClient.post(this.url+"/add",alumno);
  }
  getAlumnos():Observable<Alumno[]>{
    return this.httpClient.get<Alumno[]>(this.url);
  }
  actualizarAlumno(datos):Observable<any>{
    return this.httpClient.put(this.url+"/actualizar",datos);
  }
  getAlumnosCurso(id):Observable<Alumno[]>{
    return this.httpClient.post<Alumno[]>(this.url+"/byCurso",id);
  }
  setAlumnoFotoPerfil(data):Observable<any>{
    return this.httpClient.put(this.url+"/foto",data);
  }
  getAlumnoFotoPerfil(id):Observable<any>{
    return this.httpClient.post(this.url+'/getfoto',id)
  }
}

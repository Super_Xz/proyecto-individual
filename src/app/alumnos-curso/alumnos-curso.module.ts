import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlumnosCursoPageRoutingModule } from './alumnos-curso-routing.module';

import { AlumnosCursoPage } from './alumnos-curso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlumnosCursoPageRoutingModule
  ],
  declarations: [AlumnosCursoPage]
})
export class AlumnosCursoPageModule {}
